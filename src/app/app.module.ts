import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NewComponent } from './new-component/new-component.component';
import { SecondComponent } from './second-new-component/second-new-component.component';
import { ThirdComponentComponent } from './third-component/third-component.component';
import { FourtyComponentComponent } from './fourty-component/fourty-component.component';

@NgModule({
  declarations: [
    AppComponent, 
    NewComponent,
    SecondComponent,
    ThirdComponentComponent,
    FourtyComponentComponent  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
