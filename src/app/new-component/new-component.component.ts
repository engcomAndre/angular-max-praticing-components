import {Component} from '@angular/core';

@Component({
    selector : "app-new-component",
    template : `
        <p>This is a new component!!</p>
    `,
    styles : [
        `p{
            padding : 20px;
            border  : 1px solid red;
            background-color : lightblue;
        }`
    ]

})
export class NewComponent{

}