import { Component } from '@angular/core';


@Component({
    selector : "app-second-component",
    template : `
        <div>
            <p>This is this second component!!!</p>
        </div>
    `,
    styles :[
        `
        p{
            border : 1px solid blue;
            background-color : lightblue;
            padding : 20px;        
        }  
        `
    ]    
})
export class SecondComponent{

}